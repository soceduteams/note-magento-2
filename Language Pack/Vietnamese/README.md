1. Up file vi_VN.csv lên thư mục public
2. Install Vietnamese pack:

Develop mode
```
cd /home/nginx/domains/domain/public
wget https://bitbucket.org/soceduteams/note-magento-2/raw/af5cbebeb1fd960755b510281be7dd2e185b2892/Language%20Pack/Vietnamese/vi_VN233.csv
php bin/magento i18n:pack -m replace -d vi_VN233.csv vi_VN
php bin/magento setup:static-content:deploy -f vi_VN
php bin/magento cache:flush

```
Product mode:
```
cd /home/nginx/domains/domain/public
wget https://bitbucket.org/soceduteams/note-magento-2/raw/af5cbebeb1fd960755b510281be7dd2e185b2892/Language%20Pack/Vietnamese/vi_VN233.csv
php bin/magento i18n:pack -m replace -d vi_VN233.csv vi_VN
php bin/magento setup:static-content:deploy -f vi_VN
php bin/magento cache:flush
php bin/magento cache:enable

```

Export Translate in magento2
```

php bin/magento i18n:collect-phrases -o "/home/nginx/domains/wbuy.pro/public/vi_VN.csv" -m

php bin/magento i18n:pack -o "/home/nginx/domains/wbuy.pro/public/vi_VN.csv" vi_VN


php bin/magento i18n:pack -m replace -d vi_VN.csv vi_VN
php bin/magento setup:static-content:deploy -f vi_VN
php bin/magento cache:flush




php bin/magento cache:flush
php bin/magento cache:enable
php bin/magento cache:disable
php bin/magento cache:clean
php bin/magento cache:status

php bin/magento i18n:pack -m merge vi_VN.csv vi_VN

Test
----
```



find var vendor pub/static var/page_cache var/cache var/session pub/media app/etc -type f -exec chmod u+w {} \; && find var vendor pub/static pub/media var/page_cache var/cache var/session app/etc -type d -exec chmod u+w {} \; && chmod u+x bin/magento

rm -rf  var/cache/*  var/session/*  var/page_cache/*  pub/static/* var/view_preprocessed/pub/*

php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento cache:status
php bin/magento cache:flush
find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
find var vendor pub/static pub/media app/etc -type f -exec chmod g+w {} \;
find var vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} \;
chown -R nginx:nginx *
chmod u+x bin/magento

php bin/magento cache:flush
php bin/magento setup:static-content:deploy -f vi_VN
php bin/magento setup:static-content:deploy -f --area adminhtml




rm -rf  var/cache/*  var/session/*  var/page_cache/*  pub/static/*

```
